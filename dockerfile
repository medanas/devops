FROM node:alpine
WORKDIR /usr/app/src
ADD index.js .
ADD package.json .
RUN npm install --only=production
ENV PORT=80
CMD ["npm", "start"]