process.env.NODE_ENV = 'test'

let chaiHttp = require('chai-http'),
    chai = require('chai'),
    expect = chai.expect,
    app = require('../'),
    cc = ''

chai.use(chaiHttp)
chai.should()

describe('Credit Card API', () => {
    it('Should be running', (done) => {
        chai.request(app)
            .get('/')
            .end((_err, res) => {
                res.should.have.status(200)
                done()
            })
    })

    it('Should be up and healthy', (done) => {
        chai.request(app)
            .get('/healthz')
            .end((_err, res) => {
                res.should.have.status(200)
                done()
            })
    })

    it('Should fail with a wrong given CC number: 1234-5678', (done) => {
        cc = '1234-5678'
        chai.request(app)
            .get('/verify/' + cc)
            .end((_err, res) => {
                res.should.have.status(400)
                expect(JSON.stringify(res.body)).to.contain('must contain 16 digits')
                done()
            })
    })

    it('Should fail with a wrong given CC number: ABCD 1234 EFGH 5678', (done) => {
        cc = 'ABCD 1234 EFGH 5678'
        chai.request(app)
            .get('/info/' + cc)
            .end((_err, res) => {
                res.should.have.status(400)
                expect(JSON.stringify(res.body)).to.contain('must contain 16 digits')
                done()
            })
    })

    it('Should NOT be valid with a given CC number: 1234-5678-9009-8765', (done) => {
        cc = '1234-5678-9009-8765'
        chai.request(app)
            .get('/verify/' + cc)
            .end((_err, res) => {
                res.should.have.status(200)
                expect(JSON.stringify(res.body)).to.contain('false')
                done()
            })
    })

    it('Should be valid with a given CC number: 4000-1234-5678-9010', (done) => {
        cc = '4000-1234-5678-9010'
        chai.request(app)
            .get('/verify/' + cc)
            .end((_err, res) => {
                res.should.have.status(200)
                expect(JSON.stringify(res.body)).to.contain('true')
                done()
            })
    })

    it('Should return Visa with a given CC number: 4000-1234-5678-9010', (done) => {
        cc = '4000-1234-5678-9010'
        chai.request(app)
            .get('/info/' + cc)
            .end((_err, res) => {
                res.should.have.status(200)
                expect(JSON.stringify(res.body)).to.contain('Visa')
                done()
            })
    })
})