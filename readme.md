# Sample DevOps Project

## Linting

[Linting](https://en.wikipedia.org/wiki/Lint_(software)) is a process of analysing code for potential errors or code style violation.

[ESLint](https://eslint.org/) (for JavaScript) or [TSLint](https://palantir.github.io/tslint/) (for TypeScript) are the tools available for code linting.

Check the following files : 
* .eslintrc.json

## Testing & Code Coverage

[Software Testing](https://en.wikipedia.org/wiki/Software_testing) is a process of testing a software in order to detect issues

[Code Coverage](https://en.wikipedia.org/wiki/Code_coverage) is a measure of how much of a software is being tested

[Mocha](https://mochajs.org/) and [Istanbul or NYC](https://istanbul.js.org/) are tools of testing and code coverage

Check the following files : 
* test/index.js
* .nycrc

## Code Quality

Code Quality, also known as [Software Quality](https://en.wikipedia.org/wiki/Software_quality) is a set of analysis and measurements based on linting, code coverage, vulnerability detections and other things.

[SonarQube](https://www.sonarqube.org/) is a code quality and analysis tool

Check the following files : 
* TODO

## Packaging and Deployment

TODO

## Rolling updates and Roll backs

TODO

## Security

TODO: Helmet