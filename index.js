let bodyParser = require('body-parser'),
    cc = require('card-validator'),
    express = require('express'),
    helmet = require('helmet'),
    morgan = require('morgan'),
    cors = require('cors'),
    app = express()

let port = process.env.PORT || 8000

let validateInput = (ccNumber) => {
    return ccNumber && String(ccNumber).length === 16 && !isNaN(ccNumber)
}

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(helmet())
app.use(cors({
    origin: '*.thetestpad.com',
    methods: 'GET'
}))

if (process.env.NODE_ENV !== 'test') {
    app.use(morgan('common'))
}

app.get('/healthz', (_req, res) => {
    res.status(200).json({
        status: 'ready'
    })
})

app.get('/', (_req, res) => {
    res.status(200).json({
        message: 'Welcome to Credit Card API v1.0',
        details: 'To use the API, please send a GET request to /verify/:ccNumber or /info/:ccNumber'
    })
})

app.get('/verify/:ccNumber', (req, res) => {
    let ccNumber = req.params.ccNumber.replace(/-|\s/g, '')

    if (validateInput(ccNumber)) {
        res.status(200).json({
            valid: cc.number(ccNumber).isPotentiallyValid
        })
    }
    else {
        res.status(400).json({
            error: 'Credit Card number is required, and must contain 16 digits'
        })
    }
})

app.get('/info/:ccNumber', (req, res) => {
    let ccNumber = req.params.ccNumber.replace(/-|\s/g, '')

    if (validateInput(ccNumber)) {
        res.status(200).json(cc.number(ccNumber).card)
    }
    else {
        res.status(400).json({
            error: 'Credit Card number is required, and must contain 16 digits'
        })
    }
})

app.listen(port, () => {
    console.log('Server started, listening to port: ' + port)
})

module.exports = app